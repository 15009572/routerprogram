#ifndef ADMIN_H
#define ADMIN_H

#include <string>

#include "Account.h"
#include <fstream>

class Admin : public Account
{
public:
	Admin();
	~Admin();

	void saveInfo();
	std::string getAccountType();

	void setName(std::string);
	void setPassword(std::string);
	std::string getName();
	std::string getPassword();
	void connectDevice(Router&);
	void disconnectDevice(Router);
	Device getDevice();
	bool getConnection();
	std::string getConnectionString();
	void displayDeviceStats();
private:
	std::string _name, _password;
};

#endif // !ADMIN_H
