#include "Logger.h"



Logger::Logger()
{

	if (std::ifstream("routerLog.txt"))
	{
		// file exists
	}
	else
	{
		std::ofstream file;
		file.open("routerLog.txt");
		file << "NAME		LOAD	DEVICE_COUNT	TEMP	WIRED_COUNT		WIRELESS_COUNT\n";
		file.close();
	}
}


Logger::~Logger()
{
}

void Logger::log(Router r)
{
	// log router data

	std::ofstream file;
	file.open("routerLog.txt", std::ios::app);
	if (file.is_open())
	{
		file << r.getName()
			<< "	"
			<< r.getCurrentLoad()
			<< "	"
			<< r.getCurrentDeviceCount()
			<< "		"
			<< r.getCurrentTemp()
			<< "	"
			<< r.getWiredCount()
			<< "			"
			<< r.getWirelessCount()
			<< "\n";
	}


	file.close();
}

void Logger::read()	
{
	std::ifstream file;
	std::string line;
	file.open("routerLog.txt");
	if (file.is_open())
	{
		system("cls");
		while (!file.eof())
		{
			getline(file, line);
			std::cout << line << std::endl;
			Sleep(100);
		}
	}
	file.close();
}
