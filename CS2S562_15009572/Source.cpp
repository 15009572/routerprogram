#include <iostream>
#include <Windows.h>
#include <conio.h>

#include "DisplayOutput.h"
#include "Admin.h"
#include "RegisteredUser.h"
#include "Logger.h"

using namespace std;

bool getInput(int*);

int main()
{
	DisplayOutput display;
	list<Router*> routers;
	Logger logger;
	std::string deviceNames[4] = { "Laptop", "Desktop", "Tablet", "Mobile" };

	Account* activeAccount = display.displayLogin();

	routers.push_back(new Router("CISCO1921/K9", 100));

	/* Add Device(s) to Router(s) */
	for (list<Router*>::iterator routItr = routers.begin(); routItr != routers.end(); routItr++)
	{
		srand(time(NULL));
		int devicesToAdd = rand() % 100 + 1;
		for(int i = 0; i < devicesToAdd; i++)
			{
				(*routItr)->addDevice(Device(deviceNames[rand() % 4 + 0], rand() % 2 + 0, 10000));
			}
	} 

	list<Router*>::iterator routItr = routers.begin();

	/* While user hasn't closed program */
	while (true)
	{
		int userChoice = 0;
		int logCount = 0;
		while (!getInput(&userChoice) && logCount != 15)
		{
			(*routItr)->update();
			display.update(*(*routItr));

			display.displayMenu(activeAccount);

			Sleep(500);
			logCount++;
			system("cls");
		}
		
		// get input
		// clear screen

		display.pollKeyPress(userChoice, activeAccount, (*routItr), logger); // key was pressed

		logger.log(*(*routItr));
	}

	system("PAUSE");
	return 0;
}


bool getInput(int* i)
{
	if (_kbhit())
	{
		*i = _getch(); // is an ASCII char need to convert to decimal
		return true;
	}
	return false;
}