#ifndef REGISTERED_USER_H
#define REGISTERED_USER_H

#include <iostream>
#include <fstream>

#include "User.h"

class RegisteredUser : public User
{
public:
	RegisteredUser();
	~RegisteredUser();

	void saveInfo();

	void setName(std::string);
	void setPassword(std::string);
	std::string getName();
	std::string getPassword();

	std::string getAccountType();

	void connectDevice(Router&);
	void disconnectDevice(Router);

	void toggleWifi();
	void togglePower();

	bool getConnection();
	std::string getConnectionString();

	void displayDeviceStats();

	Device getDevice();

private:
	std::string _name, _password, _connection;
	Device* userDevice;
	bool deviceConnected;
};

#endif // !REGISERED_USER_H