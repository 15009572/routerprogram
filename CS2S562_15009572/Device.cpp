#include "Device.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>


Device::Device()
{
}

Device::Device(std::string s, bool w, int p)
{
	_name = s;
	_packets = 0;
	_powered = true;
	_isWired = w;
	_MAX_PACKETS = p;
	genMacAddr();
}


Device::~Device()
{
}

void Device::update()
{
	// more chance to increase
	srand(time(NULL));
	int y = rand() % 2 + 1;
	if (_powered)
	{
		if (y == 1)
			increaseLoad(rand() % _MAX_PACKETS + _MIN_PACKETS);
		else if(y == 2)
			decreaseLoad(rand() % _MAX_PACKETS + _MIN_PACKETS);
	}
}

void Device::increaseLoad(int p)
{
	if (_packets < _MAX_PACKETS)
		if (_packets + p > _MAX_PACKETS)
			increaseLoad(p / 2);
		else
			_packets += p;
	else
		_packets = _MAX_PACKETS;
}

void Device::decreaseLoad(int p)
{
	if (_packets >= _MIN_PACKETS)
		if (_packets - p < _MIN_PACKETS)
			decreaseLoad(p / 2);
		else
			_packets -= p;
	else
		_packets = _MIN_PACKETS;
}

char Device::assignLetter(int x)
{
	switch (x)
	{
		case 10: return 'A';
		case 11: return 'B';
		case 12: return 'C';
		case 13: return 'D';
		case 14: return 'E';
		case 15: return 'F';
		default: return NULL;
	}
}

void Device::genMacAddr()
{
	/* Make macAddr unique */
	for (int i = 0; i < 12; i++)
	{
		if (i > 0 ? (i) % 2 == 0 : 0)
			_macAddr += ":";

		int x = rand() % 16;

		if (x>9)
			_macAddr += assignLetter(x); // potential to return NULL
		else
			_macAddr += std::to_string(x);
	}
}

void Device::setMacAddr(std::string s)
{
	_macAddr = s;
}

std::string Device::getMacAddr()
{
	return _macAddr;
}

void Device::togglePowered()
{
	if (_powered)
		_powered = false;
	else
		_powered = true;
}

bool Device::getIsWired()
{
	return _isWired;
}

void Device::toggleIsWired()
{
	if (_isWired)
		_isWired = false;
	else
		_isWired = true;
}

bool Device::getIsPowered()
{
	return _powered;
}

int Device::getPackets()
{
	return _packets;
}

void Device::setPackets(int p)
{
	_packets = p;
}

std::string Device::getName()
{
	return _name;
}

void Device::setName()
{
	_name = "Mobile";
}

bool Device::operator==(Device d)
{
	return this->getMacAddr() == d.getMacAddr();
}
