#ifndef USER_H
#define USER_H

#include <string>

#include "Account.h"

class User : public Account
{
public:
	User();
	~User();

	void saveInfo();
	void connectDevice(Router&);
	void disconnectDevice(Router);
	std::string getAccountType();

	void setName(std::string);
	void setPassword(std::string);
	std::string getName();
	std::string getPassword();

	Device getDevice();
	bool getConnection();
	std::string getConnectionString();

	void displayDeviceStats();

protected:
	int _packetLimit;
	bool deviceConnected;
	std::string _connection;

private:
	Device* userDevice;
};

#endif // !USER_H