#ifndef ACCOUNT_H
#define ACCOUNT_H

#include "Device.h"
#include "Router.h"

class Account
{
public:
	Account();
	~Account();
	virtual void saveInfo() = 0;
	virtual std::string getAccountType() = 0;

	virtual void setName(std::string) = 0;
	virtual void setPassword(std::string) = 0;
	virtual std::string getName() = 0;
	virtual std::string getPassword() = 0;
	virtual void connectDevice(Router&) = 0;
	virtual void disconnectDevice(Router) = 0;
	virtual Device getDevice() = 0;
	virtual bool getConnection() = 0;
	virtual std::string getConnectionString() = 0;
	virtual void displayDeviceStats() = 0;

protected:
	std::string _accountType, _name, _password;
	std::string deviceNames[4] = { "Laptop", "Desktop", "Tablet", "Mobile" };
};

#endif // !ACCOUNT_H
