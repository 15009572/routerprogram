#ifndef ROUTER_H
#define ROUTER_H

#include <vector>
#include <list>
#include <iterator>
#include <string>
#include <time.h>
#include <iostream>
#include <Windows.h>

#include "BaseObject.h"
#include "Device.h"


class Router : public BaseObject
{
public:
	Router();
	Router(std::string, int);
	~Router();
	void update(); // called every tick
	void updateDeviceConnections();
	void updateTemp();

	void updateAverageLoad();
	double getAverageLoad();

	void updateAverageDeviceCount();
	double getAverageDeviceCount();

	void addDevice(Device);
	void removeDevice(Device);
	void removeDevice();

	int getCurrentLoad();
	int getCurrentTemp();
	int getCurrentDeviceCount();
	int getMaxDeviceCount();

	void increaseTemp(int);
	void decreaseTemp(int);	

	void toggleWifi();
	bool getWifiState();
	std::string getWifiStatus();

	void togglePower();
	bool getPowerStatus();

	int getWiredCount();
	int getWirelessCount();

	std::string getName();
	void setName();

	void setUserDevice(Device);
	Device getUserDevice();
private:
	std::list<Device> devices; // holds all devices
	Device userDevice;
	std::string deviceNames[4] = { "Laptop", "Desktop", "Tablet", "Mobile" };

	/* Let admin change load threshold */
	int32_t _MAX_LOAD = 100000;
	int16_t const _MIN_LOAD = 0;
	int16_t const _MAX_TEMP = 80;
	int16_t const _MIN_TEMP = 0;
	bool _wifiEnabled, _powered;
	// using secure integers
	int32_t _temp, _connectedDevicesCount, _maxConnections, _currentLoad, _prevLoad, _wiredConn, _wirelessConn, _averageLoad, _averageDeviceCount;
	std::string _name;
};

#endif // !ROUTER_H