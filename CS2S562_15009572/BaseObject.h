#ifndef BASEOBJECT_H
#define BASEOBJECT_H

#include <string>

class BaseObject
{
public:
	BaseObject();
	~BaseObject();

	virtual void update() = 0;

	virtual std::string getName() = 0;
	virtual void setName() = 0;

private:

};

#endif // !BASEOBJECT_H