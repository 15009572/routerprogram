#include "RegisteredUser.h"

RegisteredUser::RegisteredUser()
{
	_accountType = "Registered User";
	_packetLimit = 20000;
	userDevice = new Device(deviceNames[rand() % 4 + 0], rand() % 2 + 0, _packetLimit);
	deviceConnected = false;
	_connection = "Disconnected";
}

RegisteredUser::~RegisteredUser()
{
}

void RegisteredUser::saveInfo()
{
	// Writes _name & _password to file
	std::ofstream file;
	file.open("account.txt", std::ios::app);
	if (file.is_open())
	{
		file << _name;
		file << ":";
		file << _password + "\n";
	}
	file.close();
}

void RegisteredUser::setName(std::string s)
{
	_name = s;
}

void RegisteredUser::setPassword(std::string s)
{
	_password = s;
}

std::string RegisteredUser::getName()
{
	return _name;
}

std::string RegisteredUser::getPassword()
{
	return _password;
}

std::string RegisteredUser::getAccountType()
{
	return _accountType;
}

void RegisteredUser::connectDevice(Router& r)
{
	r.addDevice(*userDevice);
	r.setUserDevice(*userDevice);
	//r.addUser(this);
	deviceConnected = true;
	_connection = "Connected";
}

void RegisteredUser::disconnectDevice(Router r)
{
	r.removeDevice(*(userDevice));
	deviceConnected = false;
	_connection = "Disconnected";
}

void RegisteredUser::toggleWifi()
{
	userDevice->toggleIsWired();
}

void RegisteredUser::togglePower()
{
	userDevice->togglePowered();
}

Device RegisteredUser::getDevice()
{
	return (*userDevice);
}

bool RegisteredUser::getConnection()
{
	return deviceConnected;
}

std::string RegisteredUser::getConnectionString()
{
	return _connection;
}

void RegisteredUser::displayDeviceStats()
{
	if (getDevice().getIsPowered())
	{
		std::cout << "Device is currently " << _connection << std::endl;
		std::cout << "Packet limit is set to " << _packetLimit << std::endl;
		std::cout << "Your device is a " << getDevice().getName() << std::endl;
		std::cout << "Device Mac Address is " << getDevice().getMacAddr() << std::endl;
		if (getDevice().getIsWired())
			std::cout << "Device is connected via Ethernet." << std::endl;
		else
			std::cout << "Device is connected via WiFi." << std::endl;
	}
	else
	{
		std::cout << "Device is unpowered." << std::endl;
	}
}
