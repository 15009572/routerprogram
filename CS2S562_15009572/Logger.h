#ifndef LOGGER_H
#define LOGGER_H

#include "Router.h"
#include <fstream>

class Logger
{
public:
	Logger();
	~Logger();

	void log(Router);
	void read();
};

#endif // ! LOGGER_H