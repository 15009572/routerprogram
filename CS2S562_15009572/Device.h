#ifndef DEVICE_H
#define DEVICE_H

#include <string>

#include "BaseObject.h"

class Device : public BaseObject
{
public:
	Device();
	Device(std::string, bool, int);
	~Device();

	void update(); // called every tick

	void increaseLoad(int);
	void decreaseLoad(int);

	char assignLetter(int);
	void genMacAddr();

	void setMacAddr(std::string);
	std::string getMacAddr();

	void togglePowered();

	bool getIsWired();

	void toggleIsWired();

	bool getIsPowered();
	
	int getPackets();
	void setPackets(int);

	std::string getName();
	void setName();

	bool operator==(Device);

private:	
	int _MAX_PACKETS;
	int const _MIN_PACKETS = 0;
	bool _powered, _isWired;
	int _packets;

	std::string _name, _macAddr;
};

#endif // !DEVICE_H
