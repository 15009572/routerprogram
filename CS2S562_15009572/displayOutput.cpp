#include "DisplayOutput.h"



DisplayOutput::DisplayOutput()
{
}


DisplayOutput::~DisplayOutput()
{
}

void DisplayOutput::update(Router r)
{
	if (r.getPowerStatus())
	{
		cout << "Current Router Selected: " << r.getName() << endl;
		cout << "Current Load: " << r.getCurrentLoad() << endl;
		cout << "Current Temperature: " << r.getCurrentTemp() << endl;
		cout << "Devices Connected: " << r.getCurrentDeviceCount() << endl;
		cout << "Room for another " << r.getMaxDeviceCount() - r.getCurrentDeviceCount() << " devices." << endl;
		cout << "WiFi is " << r.getWifiStatus() << " on this router" << endl;
		cout << r.getWiredCount() << " devices using wired connection." << endl;
		if (r.getWifiState())
			cout << r.getWirelessCount() << " devices using wireless connection." << endl;
		cout << "Average Load is: " << r.getAverageLoad() << endl;
	}
	else
		cout << "Router " << r.getName() << " is powered off." << endl;
}

void DisplayOutput::displayMenu(Account* a)
{
	cout << endl;
	cout << "You are currently a " << a->getAccountType() << endl;
	cout << "Select one of the following options: " << endl << endl;

	if (a->getAccountType() == "User")
	{
		a->displayDeviceStats();
		cout << "\n";
		cout << "[1] Connect a device." << endl;
		cout << "[2] Disconnect a device." << endl;
		cout << "[9] Quit Program." << endl;
	}
	else if (a->getAccountType() == "Registered User")
	{
		a->displayDeviceStats();
		cout << "[1] Connect a device." << endl;
		cout << "[2] Disconnect a device." << endl;
		if (a->getConnection())
		{
			cout << "[3] Toggle WiFi/Ethernet." << endl;
			cout << "[4] Toggle Device Power." << endl;
		}
		cout << "[9] Quit Program." << endl;
	}
	else if (a->getAccountType() == "Admin")
	{
		a->displayDeviceStats();
		cout << "[1] Change Router Name." << endl;
		cout << "[2] Toggle Router Power." << endl;
		cout << "[3] Toggle Router Wifi." << endl;
		cout << "[4] Read Log." << endl; // open up new interface for logging?
		cout << "[9] Quit Program." << endl;
	}
}

Account* DisplayOutput::displayLogin()
{
	cout << "Sign in as a: " << endl;
	cout << "[1] User." << endl;
	cout << "[2] Registered User." << endl;
	cout << "[3] Admin." << endl;

	Account* a = getUserAccountType();
	if (a->getAccountType() == "User")
	{
		return a;
	}
	else if (a->getAccountType() == "Registered User")
	{
		cout << "Do you already have an account?" << endl;
		cout << "[1] Yes" << endl;
		cout << "[2] No" << endl;
		getInput(a);
		return a;
	}
	else if (a->getAccountType() == "Admin")
	{
		cout << "Do you already have an account?" << endl;
		cout << "[1] Yes" << endl;
		cout << "[2] No" << endl;
		getInput(a);
		return a;
	}
}

Account* DisplayOutput::getUserAccountType()
{
	std::string userInput;
	int validInt = 0;

	while (true)
	{
		getline(cin, userInput);

		stringstream s(userInput);
		if (s >> validInt)
		{

			switch (validInt)
			{
				case 1:
				{
					User* u = new User();
					return u;
				}
				case 2:
				{
					RegisteredUser* r = new RegisteredUser();
					return r;
				}
				case 3:
				{
					Admin* a = new Admin();
					return a;
				}
			}
		} // end if
		cout << "Invalid Choice: Please try again." << endl;
	}
}

void DisplayOutput::getInput(Account* a)
{
	std::string userInput;
	int validInt = 0;
	while (true)
	{
		getline(cin, userInput);
		stringstream s(userInput);
		if (s >> validInt)
		{
			switch (validInt)
			{
			case 1: login(a); return;
			case 2: createAccount(a); return;
			}
		}
	}
}

void DisplayOutput::login(Account* a)
{
	attemptLogin:
	std::string username, password;
	int const MAX_ATTEMPTS = 3;
	int attempts = 0;
	cout << "Enter Username: ";
	getline(cin, username);
	if (!hasSpecialChar(username))
	{
		if (doesUsernameExist(username, a))
		{
			while (attempts < MAX_ATTEMPTS) // 3 tries to get password correct
			{
				cout << "Enter Password: ";
				getline(cin, password);
				if (correctLogin(username, password, a))
				{
					// successfull
					cout << "Login Successful!" << endl;
					Sleep(3000);
					a->setName(username);
					a->setPassword(password);
					return;
				}
				cout << "Incorrect Password. Attempts remaining: " << MAX_ATTEMPTS - (attempts + 1) << endl;
				attempts++;
			}
			// unsuccessful login
			cout << "Unsuccessful login -- exiting program.";
			Sleep(3000);
			exit(0);
		}
		else
		{
			cout << "Cannot find Username: " << username << endl;
			goto attemptLogin;
		}
	}
	else
	{
		cout << "\n\nSpecial char entered, please try again!" << endl;
		goto attemptLogin;
	}
}

void DisplayOutput::createAccount(Account* a)
{
	accountCreation:
	std::string username;
	std::string password = "password";

	cout << "\nOnly user number (0-9) or characters (a-Z)." << endl;
	cout << "Enter Username: ";
	getline(cin, username);
	if (!hasSpecialChar(username))
	{
		// check if username exists
		if (doesUsernameExist(username, a))
		{
			cout << "Username already taken!" << endl;
			goto accountCreation;
		}
		a->setName(username);

		cout << "Enter Password: ";
		getline(cin, password);
		a->setPassword(password);

		a->saveInfo();
	}
	else
	{
		cout << "\n\nSpecial char entered, please try again!" << endl;
		goto accountCreation;
	}
}

bool DisplayOutput::doesUsernameExist(std::string s, Account* a)
{
	ifstream file;
	std::string line, username;
	int nameSize = 0;

	if (a->getAccountType() == "Admin")
		file.open("admin.txt");
	else
		file.open("account.txt");

	/* 
	Parsing the file securely.
	Before interacting with the file stream I am making sure the file has opened without any errors.
	*/
	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, line);

			for (int i = 0; i < line.size(); i++)
			{
				if (line[i] == ':')
					for (int x = 0; x < nameSize; x++)
						username += line[x];
				nameSize++;
			}
			if (username == s)
				return true;
			else
				username = "";
			nameSize = 0;
		}
	}

	/*
	Closing the file stream
	*/
	file.close();
	return false;
}

bool DisplayOutput::correctLogin(std::string u, std::string p, Account* a)
{
	ifstream file;
	std::string line, username, password;
	int nameSize = 0;

	if (a->getAccountType() == "Admin")
		file.open("admin.txt");
	else
		file.open("account.txt");

	if (file.is_open())
	{
		while (!file.eof())
		{
			getline(file, line);

			for (int i = 0; i < line.size(); i++)
			{
				if (line[i] == ':')
				{
					for(int x = 0; x < nameSize; x++)
						username += line[x];
					for (int x = nameSize + 1; x < line.size(); x++)
						password += line[x];
				}
				nameSize++;
			}

			if (username == u)
				if (password == p)
					return true;
				else
					password = "";
			else
			{
				username = "";
				password = "";
			}
			nameSize = 0;
		}
	}
	file.close();
	return false;
}

bool DisplayOutput::hasSpecialChar(std::string s)
{
	if (s.find(':') != std::string::npos)
		return true;
	if (s.find(';') != std::string::npos)
		return true;
	if (s.find('"') != std::string::npos)
		return true;
	if (s.find('$') != std::string::npos)
		return true;
	if (s.find('%') != std::string::npos)
		return true;
	if (s.find('^') != std::string::npos)
		return true;
	if (s.find('@') != std::string::npos)
		return true;
	if (s.find('!') != std::string::npos)
		return true;
	if (s.find('_') != std::string::npos)
		return true;
	if (s.find('-') != std::string::npos)
		return true;
	if (s.find('*') != std::string::npos)
		return true;
	if (s.find('(') != std::string::npos)
		return true;
	if (s.find(')') != std::string::npos)
		return true;
	if (s.find('+') != std::string::npos)
		return true;
	if (s.find('\\') != std::string::npos)
		return true;
	if (s.find('/') != std::string::npos)
		return true;
	if (s.find('?') != std::string::npos)
		return true;
	if (s.empty())
		return true;
	// lol
	return false;
}

void DisplayOutput::pollKeyPress(int keyPress, Account* a, Router* r, Logger l)
{
	/*
	Parses the keypress
	If the keypress isn't an available option the fucntion returns a nullptr
	*/


	if (a->getAccountType() == "User")
	{
		switch (ASCIItoDec(keyPress))
		{
		case 1: a->connectDevice(*r); return;
		case 2: a->disconnectDevice(*r); return;
		case 9: exit(0); return;
		case 11: break; // user entered out of bound char
		}
	}
	else if (a->getAccountType() == "Registered User")
	{
		switch (ASCIItoDec(keyPress))
		{
		case 1: a->connectDevice(*r); return;
		case 2: a->disconnectDevice(*r); return;
		case 3: a->getDevice().toggleIsWired(); return;
		case 4: a->getDevice().togglePowered(); return;
		case 9: exit(0); return;
		case 11: break; // user entered out of bound char
		}
	}
	else if (a->getAccountType() == "Admin")
	{
		switch (ASCIItoDec(keyPress))
		{
		case 1: r->setName(); return;
		case 2: r->togglePower(); return;
		case 3: r->toggleWifi(); return;
		case 4: l.read(); system("PAUSE"); return; // read log
		case 9: exit(0); return;
		case 11: break; // user entered out of bound char
		}
	}
}

int DisplayOutput::ASCIItoDec(int i)
{
	if (i < 48)
		return 11;

	if ((i - 48) < 1 && (i - 48) > 9)
		return 11;

	return i - 48;
}

