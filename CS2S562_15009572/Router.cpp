#include "Router.h"


Router::Router()
{
}

Router::Router(std::string s, int c)
{
	_name = s;
	_connectedDevicesCount = 0;
	_maxConnections = c;
	_currentLoad = 0;
	_temp = 50;
	_wifiEnabled = true;
	_powered = true;
	_wiredConn = 0;
	_wirelessConn = 0;
	_averageLoad = 0;
	_averageDeviceCount = 0;
}


Router::~Router()
{
	devices.clear();
}

void Router::update()
{
	if (getPowerStatus()) // if router is on
	{
		updateDeviceConnections(); // disconnect and connect random amount of devices

		// Reset load to get accurate load number
		_prevLoad = _currentLoad;
		_currentLoad = 0;
		for (std::list<Device>::iterator devItr = devices.begin(); devItr != devices.end(); devItr++)
		{
			devItr->update();
			if (_wifiEnabled) // get wired and wireless load
				_currentLoad += devItr->getPackets();
			else
				if (devItr->getIsWired()) // get wired only
					_currentLoad += devItr->getPackets();
		}

		updateTemp();

		updateAverageDeviceCount();
		updateAverageLoad();
	}
}

void Router::updateDeviceConnections()
{
	srand(time(NULL));

	int x = rand() % 10 + 1;
	int d = rand() % _connectedDevicesCount + 0;
	if (x >= 8)
	{
		// connect some devices
		if (_connectedDevicesCount == _maxConnections)
		{
			// cannt connect device, list already full
		}
		else
		{
			int count = rand() % (_maxConnections - _connectedDevicesCount) + 1;
			for (int i = 0; i < count; i++)
			{
				addDevice(Device(deviceNames[rand() % 4 + 0], rand() % 2 + 0, 10000));
			}

		}
	}
	else if (x <= 2)
	{
		// disconnet some devices
		int count = rand() % _connectedDevicesCount + 0;
		for (int i = 0; i < count; i++)
		{
			removeDevice();
		}
	}
}

void Router::updateTemp()
{
	if (_currentLoad > _prevLoad)
		increaseTemp(1);
	else
		decreaseTemp(1);
}

void Router::updateAverageLoad()
{
	_averageLoad += _currentLoad;
}

void Router::updateAverageDeviceCount()
{
	_averageDeviceCount += _connectedDevicesCount;
}

double Router::getAverageDeviceCount()
{
	int a = _averageDeviceCount / 10;
	_averageDeviceCount = 0;
	return a;
}

double Router::getAverageLoad()
{
	int a = _averageLoad / getAverageDeviceCount();
	_averageLoad = 0;
	return a;
}

void Router::addDevice(Device d)
{
	if (_connectedDevicesCount < _maxConnections)
	{
		devices.push_back(d);
		_connectedDevicesCount++;
	}
}

void Router::removeDevice(Device d)
{
	if (!devices.empty())
	{
		for (std::list<Device>::iterator itr = devices.begin(); itr != devices.end(); itr++)
		{
			if (*(itr) == d)
			{
				devices.erase(itr);
				_connectedDevicesCount--;
				return;
			}
		}
	}
}

void Router::removeDevice()
{
	// ATM removes last devices connected - doesnt remove user device if connected
	if (!devices.empty())
	{
		for (std::list<Device>::iterator itr = devices.begin(); itr != devices.end(); itr++)
		{
			if (*(itr) == getUserDevice())
			{
				// dont delete
			}
			else
			{
				// delete
				devices.erase(itr);
				_connectedDevicesCount--;
				return;
			}
		}
	}
}

int Router::getCurrentLoad()
{
	return _currentLoad;
}

int Router::getCurrentTemp()
{
	return _temp;
}

int Router::getCurrentDeviceCount()
{
	return _connectedDevicesCount;
}

int Router::getMaxDeviceCount()
{
	return _maxConnections;
}

void Router::increaseTemp(int t)
{
	if (_temp < _MAX_TEMP)
		if (_temp + t > _MAX_TEMP)
			increaseTemp(t / 2); // temp too high, switch off router
		else
			_temp += t;
	else
		_temp = _MAX_TEMP;
}

void Router::decreaseTemp(int t)
{
	if (_temp >= _MIN_TEMP)
		if (_temp - t < _MIN_TEMP)
			decreaseTemp(t / 2);
		else
			_temp -= t;
	else
		_temp = _MIN_TEMP;
}

void Router::toggleWifi()
{
	if (_wifiEnabled)
		_wifiEnabled = false;
	else
		_wifiEnabled = true;
}

bool Router::getWifiState()
{
	return _wifiEnabled;
}

std::string Router::getWifiStatus()
{
	if (_wifiEnabled)
		return "enabled";
	else
		return "disabled";
}

void Router::togglePower()
{
	if (getPowerStatus())
	{
		_powered = false;
		_temp = 0;
	}
	else
	{
		_powered = true;
		_temp = 50;
	}
}

bool Router::getPowerStatus()
{
	return _powered;
}

int Router::getWiredCount()
{
	for (std::list<Device>::iterator devItr = devices.begin(); devItr != devices.end(); devItr++)
	{
		if (devItr->getIsWired())
			_wiredConn++;
	}
	return _wiredConn;
}

int Router::getWirelessCount()
{
	for (std::list<Device>::iterator devItr = devices.begin(); devItr != devices.end(); devItr++)
	{
		if (!devItr->getIsWired())
			_wirelessConn++;
	}
	return _wirelessConn;
}

std::string Router::getName()
{
	return _name;
}

void Router::setName()
{
	std::string n, temp;
	int userChoice;
	temp = _name;
	system("cls");
	std::cout << "Change name from " << _name << " to: ";
	std::cin >> n;

	std::cout << "\nAre are sure you want to change the router name " << _name << " to " << n << " ?";
	std::cout << "\n[1] Yes\n[2] No\n";
	std::cin >> userChoice;

	if (userChoice == 1)
	{
		_name = n;
		std::cout << "Name changed.";
		Sleep(2000);
		return;
	}
	else if (userChoice == 2)
	{
		std::cout << "Name NOT changed.";
		Sleep(2000);
		return;
	}
	else
		std::cout << "Not a valid input, returning...";
	return;
}

void Router::setUserDevice(Device d)
{
	userDevice.setMacAddr(d.getMacAddr());
}

Device Router::getUserDevice()
{
	return userDevice;
}
