#ifndef DISPLAY_OUTPUT_H
#define DISPLAY_OUTPUT_H

#include <iostream>
#include <string>
#include <sstream>
#include <Windows.h>
#include <cstdlib>

#include "RegisteredUser.h"
#include "Admin.h"
#include "Logger.h"

using namespace std;

class DisplayOutput
{
public:
	DisplayOutput();
	~DisplayOutput();

	void update(Router);

	void displayMenu(Account*);
	Account* displayLogin();

	Account* getUserAccountType();
	void getInput(Account*);
	void login(Account*);
	void createAccount(Account*);
	bool doesUsernameExist(std::string, Account*);
	bool correctLogin(std::string, std::string, Account*);

	bool hasSpecialChar(std::string);

	void pollKeyPress(int, Account*, Router*, Logger);
	int ASCIItoDec(int);
};

#endif // !DISPLAY_OUTPUT_H