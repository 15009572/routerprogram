#include "Admin.h"



Admin::Admin()
{
	_accountType = "Admin";
}


Admin::~Admin()
{
}

void Admin::saveInfo()
{
	std::ofstream file;
	file.open("admin.txt", std::ios::app);
	if (file.is_open())
	{
		file << _name;
		file << ":";
		file << _password + "\n";
	}
	file.close();
}

std::string Admin::getAccountType()
{
	return _accountType;
}

void Admin::setName(std::string s)
{
	_name = s;
}

void Admin::setPassword(std::string s)
{
	_password = s;
}

std::string Admin::getName()
{
	return _name;
}

std::string Admin::getPassword()
{
	return _password;
}

void Admin::connectDevice(Router& r)
{
	//r.addDevice(d);
}

void Admin::disconnectDevice(Router r)
{
	//r.removeDevice(d);
}

Device Admin::getDevice()
{
	return Device();
}

bool Admin::getConnection()
{
	return false;
}

std::string Admin::getConnectionString()
{
	return "";
}

void Admin::displayDeviceStats()
{
	std::cout << "Admin does not have a device" << std::endl << std::endl;
}
