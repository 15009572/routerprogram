#include "User.h"



User::User()
{
	_accountType = "User";
	_name = "Guest";
	_connection = "Disconnected";
	_password = "";
	_packetLimit = 10000;
	userDevice = new Device(deviceNames[rand() % 4 + 0], rand() % 2 + 0, _packetLimit);
	deviceConnected = false;
}


User::~User()
{
}

void User::saveInfo()
{
}

void User::connectDevice(Router& r)
{
	r.addDevice(*userDevice);
	r.setUserDevice(*userDevice);
	//r.addUser(this);
	deviceConnected = true;
	_connection = "Connected";
}

void User::disconnectDevice(Router r)
{
	r.removeDevice(*(userDevice));
	deviceConnected = false;
	_connection = "Disconnected";
}

std::string User::getAccountType()
{
	return _accountType;
}

void User::setName(std::string s)
{
	_name = "Guest";
}

void User::setPassword(std::string)
{
	_password = "";
}

std::string User::getName()
{
	return _name;
}

std::string User::getPassword()
{
	return _password;
}

Device User::getDevice()
{
	return (*userDevice);
}

bool User::getConnection()
{
	return deviceConnected;
}

std::string User::getConnectionString()
{
	return _connection;
}

void User::displayDeviceStats()
{
	std::cout << "Device is currently " << _connection << std::endl;
	std::cout << "Packet limit is set to " << _packetLimit << std::endl;
	std::cout << "Your device is a " << getDevice().getName() << std::endl;
	std::cout << "Device Mac Address is " << getDevice().getMacAddr() << std::endl;
}
